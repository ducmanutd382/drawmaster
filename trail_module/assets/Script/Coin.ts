// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Lin from "./Lin";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Coin extends cc.Component {

    @property(cc.BoxCollider)
    collider: cc.BoxCollider = null;

    @property(cc.PhysicsBoxCollider)
    phyCollider: cc.PhysicsBoxCollider = null;
    //@property(cc.RigidBody)
    //rigid: cc.RigidBody = null;


    lin: Lin;
    bullet: Bullet;

    @property(cc.CircleCollider)
    bulletCol: cc.CircleCollider;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        var anim = this.getComponent(cc.Animation);
        var animState = anim.play();
        animState.repeatCount = Infinity;
    }



    onCollisionEnter(bulletCol: cc.CircleCollider, collider: cc.BoxCollider) {
        //this.node.destroy();
        console.log("da va cham");
        this.onPicked();

        GameManager.game.plusScore();
    }
    onPicked() {
        //this.game.plusScore();

        this.node.destroy();
    }

}
