// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import box from "./box";
import GameManager from "./GameManager";
import Tag from "./Tag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Ball extends cc.Component {

    @property(cc.CircleCollider)
    ballCol: cc.CircleCollider;

    @property(cc.BoxCollider)
    boxCol: cc.BoxCollider;

    boxx: box;
    game: GameManager;

    public static ball: Ball;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        Ball.ball = this;
    }
    start(){
        let rigid = this.node.getComponent(cc.RigidBody);
        rigid.gravityScale = 2;
    }

    onCollisionEnter(boxCol: cc.BoxCollider, ballCol: cc.CircleCollider) {

        if (boxCol.tag == Tag.value.box) {
            //Sbox.boxx.onPicked();
        }
        if(boxCol.tag == Tag.value.ball){
            
        }
        //var anim = box.boxx.getComponent(cc.Animation);
        //anim.play();
        // box.boxx.onPicked();
        

    }


    // update (dt) {}
}
