// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bullet_Explsion extends cc.Component {

    @property(cc.Node)
    Bullet_Root: cc.Node;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    onLoad(){

    }

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.body.type === cc.RigidBodyType.Dynamic){
            let other_pos = otherCollider.node.getPosition();
            let self_pos = this.Bullet_Root.getPosition();

            let force_vector = other_pos.sub(self_pos);

            force_vector.normalizeSelf();

            force_vector.mulSelf(200000);

            otherCollider.body.applyForceToCenter(force_vector);
            console.log("va cham vung nguy hiem");
        }
    }

    // update (dt) {}
}
