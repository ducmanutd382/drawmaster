// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Coin from "./Coin";
import GameManager from "./GameManager";
import Gem from "./Gem";
import Ground from "./Ground";
import Tag from "./Tag";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Bullet extends cc.Component {


    @property
    speed: number = 0;

    @property(cc.RigidBody)
    rigid: cc.RigidBody = null;

    @property(cc.CircleCollider)
    collider: cc.CircleCollider = null;

    @property(cc.Node)
    bulletExp: cc.Node;

    ground: Ground;
    game: GameManager;

    //checkBullet = true;

    public static bullet: Bullet;

    coin: Coin;
    gem: Gem;

    onLoad() {
        Bullet.bullet = this;

    }
    start(){
        //this.rigid.gravityScale = 0;
        this.rigid.active = false;
    }

    onCollisionEnter(otherCol: cc.Collider, box: cc.BoxCollider){
        if(otherCol.tag == Tag.value.player || otherCol.tag == Tag.value.ball){
            console.log("va cham voi dan roi!!!!!!!");
            //this.rigid.gravityScale = 1;
            this.rigid.active = true;
            this.scheduleOnce(function() {
                this.bulletExp.active = true;

            }, 0.2);

        }
        if(otherCol.tag == Tag.value.spine || otherCol.tag == Tag.value.sat){
            this.onPick();
        }
    }

    onPick() {
        
        this.node.destroy();
        //cc.director.loadScene('game');
        //GameManager.game.loadMap();
        //this.rigid.gravityScale;
        //this.checkBullet = false;
        //console.log("bullet con khong: " + this.checkBullet);
    }

    checkNextMap(){
        this.node.destroy();
    }


    // update (dt) {}
}
