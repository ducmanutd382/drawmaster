// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameManager from "./GameManager";

const {ccclass, property} = cc._decorator;
//const { _decorator, Component, Node,instantiate, director } = cc.;

@ccclass
export default class Lin extends cc.Component {

    @property(cc.Node)
    test: cc.Node = null;

    game: GameManager;
    lin: Lin;

    @property(cc.Node)
    bullet: cc.Node ;

    checkMove = false;
    
    temp = 1;

    angle = 0;

    public static lin: Lin;
    bull: cc.Node;
    
    public static namePlayer: number;

    start(){
        //this.bullet.active = false;
        Lin.namePlayer = GameManager.game.playerArray.length - 1;
        console.log(GameManager.game.playerArray.length -1);
        this.bullet = GameManager.game.bulletArray[Lin.namePlayer];
    }
    


    onLoad(){
        Lin.lin = this;

        this.node.on(cc.Node.EventType.TOUCH_START,this.TouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE,this.mouseFun, this);
        this.node.on(cc.Node.EventType.TOUCH_END,this.TouchEnd, this);
    }

    update(dt: number){
        
    }

    
    private TouchStart(event){
        this.positions.push(cc.v2(event.getLocation().x-cc.winSize.width/2,event.getLocation().y-cc.winSize.height/2));
    }

    private positions:cc.Vec2[] = new Array();
    private time=0;

    mouseFun(event:cc.Event.EventTouch){
        //console.log('23424');
        //console.log("AAAAAAAAAAAA");
        //console.log("event...." +  + event.getLocation());
        this.test.worldPosition.valueWorld = event.getLocation();
        //console.log("cccccccccc");
        this.checkMove = true;
        console.log("di chuyen ko: "+ this.checkMove);
        
       
        this.time+=cc.Vec2.distance(this.positions[this.positions.length-1],cc.v2(event.getLocation().x-cc.winSize.width/2,event.getLocation().y-cc.winSize.height/2))/300;
        this.positions.push(cc.v2(event.getLocation().x-cc.winSize.width/2,event.getLocation().y-cc.winSize.height/2));
        //console.log(positions[2]);

    }
    private TouchEnd(){
        //this.bullet.active = true;
        //var bull = this.node.getChildByName("Bullet");
        //this.bull.position = this.positions[0];
        //let act = cc.catmullRomTo(this.time, this.positions);
        //this.bull.runAction(act);
        
        //this.bullet.position=this.positions[this.positions.length + 1];


        this.bullet.position=this.positions[0];
        let action = cc.catmullRomTo(this.time, this.positions);
        this.bullet.runAction(action);

        //if(this.bullet.position == this.positions[])
        /*
        this.schedule(function() {
            // Here `this` is referring to the component
            console.log("toa do bullet" + this.bullet.position);
            console.log("toa do : "+ this.positions[this.positions.length-1]);
            console.log("bang nhau chua: " + (this.bullet.position.x == this.positions[this.positions.length-1].x));
        }, 5);
        
        if((this.bullet.position.x == this.positions[this.positions.length-1].x) && (this.bullet.position.y == this.positions[this.positions.length-1].y)){
            //this.bullet.position.x++;
            let act = cc.moveTo(this.time, cc.v2(this.bullet.position.x+2, this.bullet.position.y ));
            this.bullet.runAction(act);
        }
        */
        this.schedule(function(){

            //this.bullet.rotation = this.mathAng(this.positions[this.temp-1] , this.positions[this.temp]);

            this.node.angle = this.mathAng(this.positions[this.temp-1] , this.positions[this.temp]) //(this.poslast, this. node. position)
            //this.poslast = this.node.position;
            this.temp;
            //this.posin = this.positions[this.positions.length + 1];
            
        }, 0.02);

            
    }
    onDestroy(){
        this.node.off(cc.Node.EventType.TOUCH_MOVE,this.mouseFun, this);
        
        
    }
    mathAng(a: cc.Vec2, b: cc.Vec2){


        return this.angle = Math.atan((a.y - b.y)/(b.x - a.x));

        
    }

    checkNextMap(){
        this.node.destroy();
    }
    
    
}
