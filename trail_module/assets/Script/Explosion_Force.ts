// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import box from "./box";
import GameManager from "./GameManager";
import Tag from "./Tag";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Explosion_Force extends cc.Component {

    @property(cc.Node)
    Boom_Root: cc.Node;

    boxx: box;
    game: GameManager;

    onLoad(){

    }

    onBeginContact(contact, selfCollider, otherCollider){
        if(otherCollider.body.type === cc.RigidBodyType.Dynamic){
            let other_pos = otherCollider.node.getPosition();
            let self_pos = this.Boom_Root.getPosition();

            let force_vector = other_pos.sub(self_pos);

            force_vector.normalizeSelf();

            force_vector.mulSelf(200000);

            otherCollider.body.applyForceToCenter(force_vector);
        }
    }
    // update (dt) {}
}
