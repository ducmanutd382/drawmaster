// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Tag from "./Tag";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Groundd extends cc.Component {

    @property(cc.CircleCollider)
    bulletCol: cc.CircleCollider;

    @property(cc.BoxCollider)
    collider: cc.BoxCollider;

    public static ground: Groundd;
    bullet: Bullet;
    game: GameManager;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        Groundd.ground = this;
    }

    start () {

    }

    onCollisionEnter(bulletCol: cc.CircleCollider, collider: cc.BoxCollider) {
        
        //GameManager.game.checkGround();
        if (bulletCol.tag == Tag.value.bullet && collider.tag == Tag.value.ground){
            Bullet.bullet.onPick();
        }
        if(bulletCol.tag == Tag.value.ground){
            
        }
        console.log("va cham voi ground");
        
    }

    // update (dt) {}
}
