// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Tag from "./Tag";
import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Lin from "./Lin";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Wood extends cc.Component {

    lin: Lin;
    bullet: Bullet;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    onCollisionEnter(bulletCol: cc.CircleCollider, collider: cc.BoxCollider) {
        //this.node.destroy();
        if(bulletCol.tag == Tag.value.bullet){
            this.onPicked();
            console.log("da va cham voi wood");

        }

    }
    onPicked() {
        this.scheduleOnce(function() {
            this.node.destroy();
        }, 0.5);

        
    }

    // update (dt) {}
}
