// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class MoveUp extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.moveTo();
    }

    start() {

    }

    moveTo() {
        let action = cc.tween(this.node).to(1, { position: cc.v2(this.node.position.x, this.node.position.y + 600) }, { easing: 'sineOut' }).start();
        //this.move;
    }

    // update (dt) {}
}
