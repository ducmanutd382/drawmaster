// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Lin from "./Lin";
import Tag from "./Tag";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Gem extends cc.Component {

    @property(cc.BoxCollider)
    collider: cc.BoxCollider = null;

    @property(cc.PhysicsBoxCollider)
    phyCollider: cc.PhysicsBoxCollider = null;

    @property(cc.CircleCollider)
    bulletCol: cc.CircleCollider;

    bullet: Bullet;

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(bulletCol: cc.CircleCollider, collider: cc.BoxCollider) {
        //this.node.destroy();
        if(bulletCol.tag == Tag.value.bullet){
            this.onPicked();
        }
        //console.log("da va cham");
        

        GameManager.game.plusGem();
    }
    onPicked() {
        //this.game.plusScore();

        this.node.destroy();
    }

    onLoad(){
        //this.moveTo();
    }
    moveTo() {
        //let action = cc.tween(this.node).to(1, { position: cc.v2(this.node.position.x, this.node.position.y + 600) }, { easing: 'sineOut' }).start();
        //this.move;
    }

}
