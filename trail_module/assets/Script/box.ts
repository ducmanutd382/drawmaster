// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Ground from "./Ground";
import Lin from "./Lin";
import Tag from "./Tag";

const {ccclass, property} = cc._decorator;

@ccclass
export default class box extends cc.Component {


    @property(cc.PhysicsBoxCollider)
    box: cc.PhysicsBoxCollider = null;

    @property(cc.CircleCollider)
    bulletCol: cc.CircleCollider;

    @property(cc.BoxCollider)
    boxCol: cc.BoxCollider;

    public static dem = 0;
    private demBox: number[] = new Array();
    demboxx = 0;

    game: GameManager;
    bullet: Bullet;
    ground: Ground;

    boxCheck = true;
    public static boxx: box;

    onLoad () {
        box.boxx = this;

        box.dem++;

        console.log("so box: " + box.dem);
    }

    onCollisionEnter(bulletCol: cc.Collider, box: cc.BoxCollider){
        //this.bullNode = cc.find("Bullet");
        //console.log("node mau " + this.node.color);
        let bulletTemp = bulletCol.node;
        //console.log("bullet mau " + bulletTemp.color);
        
        
        
        
        if( (bulletCol.tag == Tag.value.bullet && this.node.color.equals(bulletTemp.color) == true ) || 
            bulletCol.tag == Tag.value.ball || 
            bulletCol.tag == Tag.value.expl_boom ||
            bulletCol.tag == Tag.value.axit ||
            bulletCol.tag == Tag.value.spine  ){
            //console.log("node mau " + this.node.color.toRGBValue);
            //console.log("bl mau " + bulletTemp.color.);
        
        
            this.onPicked();
            console.log("va cham voi box");
        }
        
        //this.checkNextMap();
    }

    onCollisionExit(bulletCol: cc.Collider, box: cc.BoxCollider){
        this.checkNextMap();
        
    }
    

    onPicked(){
        var anim = this.getComponent(cc.Animation);
        anim.play();
        this.scheduleOnce(function() {
            this.node.destroy();

        }, 0.02);
        box.dem--;
        console.log("so box con lai" + box.dem);

        this.boxCheck = false;
    }

    checkNextMap(){
        if(box.dem == 0){
            GameManager.indexLv += 1;
            //GameManager.game.resetMap();
            GameManager.game.PlayButton.active = true;
            GameManager.game.WinGame.active = true;
            console.log("next mappppppp");
        }
        
    }
}
