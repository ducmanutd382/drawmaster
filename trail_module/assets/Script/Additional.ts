const { ccclass, property } = cc._decorator;

@ccclass
export default class Additional extends cc.Component {

    public static halfWidth: number = -1;
    public static halfHeight: number = -1;

    onLoad() {
        Additional.halfWidth = cc.winSize.width / 2;
        Additional.halfHeight = cc.winSize.height / 2;
        let play = (node: cc.Node) => {
            node.worldPosition = {
                get value(): cc.Vec2 {
                    let pos = node.parent.convertToWorldSpaceAR(node.position);
                    return cc.v2(pos.x - Additional.halfWidth, pos.y - Additional.halfHeight);
                },
                set value(pos: cc.Vec2) {
                    node.position = node.parent.convertToNodeSpaceAR(cc.v2(pos.x + Additional.halfWidth, pos.y + Additional.halfHeight));
                },
                get valueWorld(): cc.Vec2 {
                    let pos = node.parent.convertToWorldSpaceAR(node.position);
                    return cc.v2(pos.x, pos.y);
                },
                set valueWorld(pos: cc.Vec2) {
                    node.position = node.parent.convertToNodeSpaceAR(cc.v2(pos.x, pos.y));
                },
                byCanvas(pos: cc.Vec2): cc.Vec2 {
                    let posResult = node.parent.convertToWorldSpaceAR(pos);
                    return cc.v2(posResult.x - Additional.halfWidth, posResult.y - Additional.halfHeight);
                },
                byWorld(pos: cc.Vec2): cc.Vec2 {
                    let posResult = node.parent.convertToWorldSpaceAR(pos);
                    return posResult;
                }
            };
            node.worldScale = {
                get value(): cc.Vec2 {
                    let tmp = node;
                    let scale = cc.v2(tmp.scaleX, tmp.scaleY);
                    while (tmp.name != "Canvas") {
                        tmp = tmp.parent;
                        scale = cc.v2(scale.x * tmp.scaleX, scale.y * tmp.scaleY);
                    }
                    return scale;
                },
                set value(scale: cc.Vec2) {
                    let scaleParent = node.parent.worldScale.value;
                    node.setScale(scale.x / scaleParent.x, scale.y / scaleParent.y);
                }
            };
            node.worldAngle = {
                get value(): number {
                    let tmp = node;
                    let angle = tmp.angle;
                    while (tmp.name != "Canvas") {
                        tmp = tmp.parent;
                        angle += tmp.angle;
                    }
                    return angle;
                },
                set value(angle: number) {
                    let angleParent = node.parent.worldAngle.value;
                    node.angle = angle - angleParent;
                }
            };
            node.addChild = (child: cc.Node) => {
                child.setParent(node);
                play(child);
            };
            for (let i = 0; i < node.children.length; i++) {
                play(node.children[i]);
            }
        }
        play(this.node);
        cc.randomRangeInt = (min, max) => {
            return Math.floor(Math.random() * (max - min)) + min;
        };
        cc.randomRange = (min, max) => {
            return Math.random() * (max - min) + min;
        }
    }

    // public static posWorld(pos: cc.Vec2): cc.Vec2 {
    //     if (Tool.halfWidth == -1)
    //         Tool.halfWidth = cc.winSize.width / 2;
    //     if (Tool.halfHeight == -1)
    //         Tool.halfHeight = cc.winSize.height / 2;
    //     return cc.v2(pos.x + Tool.halfWidth, pos.y + Tool.halfHeight);
    // }
    // public static xWorld(x: number): number {
    //     if (Tool.halfWidth == -1)
    //         Tool.halfWidth = cc.winSize.width / 2;
    //     return x + Tool.halfWidth;
    // }
    // public static yWorld(y: number): number {
    //     if (Tool.halfHeight == -1)
    //         Tool.halfHeight = cc.winSize.height / 2;
    //     return y + Tool.halfHeight;
    // }
    // public static SetWorldPos(node: cc.Node, pos: cc.Vec2) {
    //     if (Tool.halfWidth == -1)
    //         Tool.halfWidth = cc.winSize.width / 2;
    //     if (Tool.halfHeight == -1)
    //         Tool.halfHeight = cc.winSize.height / 2;
    //     node.position = node.parent.convertToNodeSpaceAR(cc.v2(pos.x + this.halfWidth, pos.y + this.halfHeight));
    // }
    // public static GetWorldPos(node: cc.Node): cc.Vec2 {
    //     if (Tool.halfWidth == -1)
    //         Tool.halfWidth = cc.winSize.width / 2;
    //     if (Tool.halfHeight == -1)
    //         Tool.halfHeight = cc.winSize.height / 2;
    //     let pos = node.parent.convertToWorldSpaceAR(node.position);
    //     return cc.v2(pos.x - this.halfWidth, pos.y - this.halfHeight);
    // }
    // public static GetWorldPosByNode(node: cc.Node, pos: cc.Vec2): cc.Vec2 {
    //     if (Tool.halfWidth == -1)
    //         Tool.halfWidth = cc.winSize.width / 2;
    //     if (Tool.halfHeight == -1)
    //         Tool.halfHeight = cc.winSize.height / 2;
    //     let posResult = node.parent.convertToWorldSpaceAR(pos);
    //     return cc.v2(posResult.x - this.halfWidth, posResult.y - this.halfHeight);
    // }
}
