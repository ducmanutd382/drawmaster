// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Tag from "./Tag";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Boom extends cc.Component {

    @property(cc.Node)
    Explo_force: cc.Node;

    onCollisionEnter(bulletCol: cc.Collider, box: cc.Collider){
        var anim = this.getComponent(cc.Animation);
        anim.play();
        if(bulletCol.tag == Tag.value.bullet || bulletCol.tag == Tag.value.ball || bulletCol.tag == Tag.value.box || bulletCol.tag == Tag.value.expl_boom){
            console.log("da va cham voi booooom");
            this.scheduleOnce(function() {
                this.Explo_force.active = true;
            }, 0.2);
            this.scheduleOnce(function() {
                
                this.node.destroy();
            }, 0.3);
            
        }
    }

}
