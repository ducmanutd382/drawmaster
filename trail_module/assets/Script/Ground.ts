// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import box from "./box";
import Bullet from "./Bullet";
import GameManager from "./GameManager";
import Tag from "./Tag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Ground extends cc.Component {

    bullet: Bullet;
    game: GameManager;

    public static ground: Ground;


    onLoad() {
        Ground.ground = this;
        
    }

    onCollisionEnter(bulletCol: cc.CircleCollider, collider: cc.Collider) {
        
        //GameManager.game.checkGround();
        if (bulletCol.tag == Tag.value.bullet && collider.tag == Tag.value.ground){
            Bullet.bullet.onPick();
        }

        console.log("va cham voi ground");

        
    }
    checkNextMap(){
        if(box.dem == 0){
            //this.node.destroy();
        }
    }
}
