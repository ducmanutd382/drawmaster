// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import box from "./box";
import Bullet from "./Bullet";
import Coin from "./Coin";
import Gem from "./Gem";
import Lin from "./Lin";

export enum GameStatus {
    Game_Menu = 0,
    Game_Playing,
    Game_Over
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {

    gameStatus: GameStatus = GameStatus.Game_Menu;

    @property(cc.Label)
    scoreDisplay: cc.Label = null;
    score: number = 0;

    @property(cc.Label)
    gemDisplay: cc.Label;
    gem: number = 0;
    ////////////////////////    
    ///////////////////////////
    @property(cc.Prefab)
    bullet: cc.Prefab = null;

    @property(cc.Node)
    bulletParent: cc.Node;

    @property(cc.Prefab)
    coinPre: cc.Prefab = null;

    @property(cc.Node)
    coinParent: cc.Node;

    @property(cc.Prefab)
    boxPre: cc.Prefab;

    @property(cc.Node)
    boxParent: cc.Node;

    @property(cc.Node)
    ballParent: cc.Node;

    @property(cc.Prefab)
    ballPre: cc.Prefab;

    @property(cc.Prefab)
    axitPre: cc.Prefab;

    @property(cc.Node)
    axitParent: cc.Node;

    @property(cc.Prefab)
    woodPre: cc.Prefab;

    @property(cc.Node)
    woodParent: cc.Node;

    @property(cc.Prefab)
    gemPre: cc.Prefab;

    @property(cc.Node)
    gemParent: cc.Node;

    @property(cc.Prefab)
    boomPre: cc.Prefab;

    @property(cc.Node)
    boomParent: cc.Node;

    @property(cc.Prefab)
    groundPre: cc.Prefab;

    @property(cc.Node)
    groundParent: cc.Node;

    @property(cc.TiledMap)
    boardTiledMap: cc.TiledMap;

    @property(cc.PhysicsPolygonCollider)
    colMap: cc.PhysicsPolygonCollider;

    @property(cc.PolygonCollider)
    colPolygonMap: cc.PolygonCollider;

    @property(cc.Prefab)
    colPoMap: cc.Prefab;

    @property(cc.Node)
    colPoMapParent: cc.Node;

    @property(cc.Prefab)
    player: cc.Prefab = null;

    @property(cc.Node)
    playerParent: cc.Node;

    @property(cc.Prefab)
    spinePre: cc.Prefab;

    @property(cc.Node)
    spineParent: cc.Node;

    @property(cc.Prefab)
    satPre: cc.Prefab;

    @property(cc.Node)
    satParent: cc.Node;

    @property(cc.Node)
    replayButton: cc.Node;

    @property(cc.Node)
    PlayButton: cc.Node;

    @property(cc.Node)
    GameOver: cc.Node;

    @property(cc.Node)
    StartGame: cc.Node;

    @property(cc.Node)
    InGame: cc.Node;

    @property(cc.Node)
    WinGame: cc.Node;

    @property(cc.Node)
    nextMapButton: cc.Node;

    spGameOver: cc.Sprite;

    public static game: GameManager;
    public static indexLv: number = 1;
    coins: Coin;
    boxx: box;
    gems: Gem;
    lin: Lin;
    loadMapCheck = false;

    nodeArray: cc.Node[] = new Array();
    playerArray: cc.Node[] = new Array();
    bulletArray: cc.Node[] = new Array();


    public static demBox = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().debugDrawFlags = 1;
        var manager = cc.director.getCollisionManager();
        manager.enabledDebugDraw = true;
        manager.enabled = true;
        GameManager.game = this;

        this.PlayButton.on(cc.Node.EventType.TOUCH_END, this.Btn_playButton_Clicked, this);
        this.nextMapButton.on(cc.Node.EventType.TOUCH_END, this.Btn_playButton_Clicked, this);
        this.replayButton.on(cc.Node.EventType.TOUCH_END, this.Btn_ReplayButton_Clicked, this);

        if (this.PlayButton.active = true) {
            ///////////////////////
            //GameManager.game.groundParent.active = false;
            //GameManager.game.colPoMapParent.active = false;
            //GameManager.game.colMap.addComponent = false;
        }
    }

    Btn_playButton_Clicked() {
        GameManager.game.resetMap();
        this.loadMap();

        console.log("level map: " + GameManager.indexLv);
        //this.PlayButton.active = false;
        this.StartGame.active = false;
        this.InGame.active = true;
        this.gameStatus = GameStatus.Game_Playing;

        //GameManager.game.groundParent.active = true;
        //GameManager.game.colPoMapParent.active = true;
        //this.GameOver.active = true;
    }

    Btn_ReplayButton_Clicked() {
        this.loadMap();
        this.resetMap();
    }



    start() {
        this.StartGame.active = true;
        this.InGame.active = false;
        this.WinGame.active = false;
        this.score = 0;
        this.gem = 0;
        GameManager.indexLv = 1;
        //box.dem != 0;
        console.log("da load map chua: " + this.loadMapCheck);
        ////////////////////////////////
        this.GameOver.active = false;
    }

    update(dt) {
        if (this.gameStatus !== GameStatus.Game_Playing) {
            return;
        }
    }

    plusScore() {
        this.score += 10;
        this.scoreDisplay.string = 'Coin: ' + this.score;
    }

    plusGem() {
        this.gem += 1;
        this.gemDisplay.string = 'Gem: ' + this.gem;
    }

    resetMap() {
        this.nodeArray.forEach(element => {
            element.destroy();
        });
        this.playerArray.forEach(element => {
            element.destroy();  
        });
        this.bulletArray.forEach(element => {
            element.destroy();  
        });
        box.dem = 0;
        //Lin.lin.start();
    }

    loadMap() {
        this.WinGame.active = false;
        /*cc.resources.load("Map/Lv" + GameManager.indexLv, cc.TiledMapAsset, (err, tileMapAsset: cc.TiledMapAsset) => {
            this.boardTiledMap.tmxAsset = tileMapAsset;
            console.log("hien thi tile map");
        });*/
        cc.resources.load("Map/Lv" + GameManager.indexLv, cc.JsonAsset, (err, data: cc.JsonAsset) => {

            //let lins = Lin.lin.namePlayer;

            for (let i = 0; i < data.json.player.length; i++) {
                // let tmp = this.playerNode;
                let tmp = cc.instantiate(this.player);
                //Lin.lin.namePlayer
                this.playerArray.push(tmp);
                this.playerParent.addChild(tmp);
                tmp.setPosition(data.json.player[i].pos.x, data.json.player[i].pos.y);
                console.log("toa do cua player" + data.json.player[i].pos.x, data.json.player[i].pos.y);
            }

            for (let i = 0; i < data.json.bullet.length; i++) {
                //let tmp = this.bulletNode;
                let tmp = cc.instantiate(this.bullet);
                this.bulletArray.push(tmp);
                //tmp.setParent(this.bulletParent);
                this.bulletParent.addChild(tmp);
                tmp.setPosition(data.json.bullet[i].pos.x, data.json.bullet[i].pos.y);
                tmp.color = new cc.Color(data.json.bullet[i].color.r, data.json.bullet[i].color.g, data.json.bullet[i].color.b);
            }

            for (let i = 0; i < data.json.coin.length; i++) {
                let tmp = cc.instantiate(this.coinPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.coinParent);
                tmp.setPosition(data.json.coin[i].pos.x, data.json.coin[i].pos.y);

                console.log(data.json.coin[i].pos);
            }

            for (let i = 0; i < data.json.box.length; i++) {
                let tmp = cc.instantiate(this.boxPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.boxParent);
                tmp.setPosition(data.json.box[i].pos.x, data.json.box[i].pos.y);
                tmp.color = new cc.Color(data.json.box[i].color.r, data.json.box[i].color.g, data.json.box[i].color.b);
                //console.log(data.json.box[i].color.r, data.json.box[i].color.g, data.json.box[i].color.b);
                GameManager.demBox = data.json.box.length;
            }



            console.log(" so hop trong map" + GameManager.demBox);

            //set vị trí và size của các hình khối map thuộc GROUND
            for (let i = 0; i < data.json.ground.length; i++) {
                let tmp = cc.instantiate(this.groundPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.groundParent);
                tmp.setPosition(data.json.ground[i].pos.x, data.json.ground[i].pos.y);
                tmp.setContentSize(data.json.ground[i].size.w, data.json.ground[i].size.h);
                tmp.setRotation(data.json.ground[i].rot);

            }

            //set PhysicPolygonCol và PolygonCol để thực hiện va chạm và vật lí thuộc GROUND
            let tmp = new Array();
            for (let i = 0; i < data.json.col.points.length; i++) {
                tmp.push(cc.v2(data.json.col.points[i].x, data.json.col.points[i].y))
                //this.nodeArray.push()
            }
            this.colPolygonMap.points = tmp;
            this.colMap.points = tmp;
            this.colMap.apply();


            for (let i = 0; i < data.json.ball.length; i++) {
                let tmp = cc.instantiate(this.ballPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.ballParent);
                tmp.setPosition(data.json.ball[i].pos.x, data.json.ball[i].pos.y);
            }

            for (let i = 0; i < data.json.axit.length; i++) {
                let tmp = cc.instantiate(this.axitPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.axitParent);
                tmp.setPosition(data.json.axit[i].pos.x, data.json.axit[i].pos.y);
            }

            for (let i = 0; i < data.json.wood.length; i++) {
                let tmp = cc.instantiate(this.woodPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.woodParent);
                tmp.setPosition(data.json.wood[i].pos.x, data.json.wood[i].pos.y);
                tmp.setRotation(data.json.wood[i].rot);
            }

            for (let i = 0; i < data.json.boom.length; i++) {
                let tmp = cc.instantiate(this.boomPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.boomParent);
                tmp.setPosition(data.json.boom[i].pos.x, data.json.boom[i].pos.y);
            }

            for (let i = 0; i < data.json.spine.length; i++) {
                let tmp = cc.instantiate(this.spinePre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.spineParent);
                tmp.setPosition(data.json.spine[i].pos.x, data.json.spine[i].pos.y);
                tmp.setRotation(data.json.spine[i].rot);
            }

            for (let i = 0; i < data.json.sat.length; i++) {
                let tmp = cc.instantiate(this.satPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.satParent);
                tmp.setPosition(data.json.sat[i].pos.x, data.json.sat[i].pos.y);
                tmp.setRotation(data.json.sat[i].rot);
            }


            for (let i = 0; i < data.json.gem.length; i++) {
                let tmp = cc.instantiate(this.gemPre);
                this.nodeArray.push(tmp);
                tmp.setParent(this.gemParent);
                tmp.setPosition(data.json.gem[i].pos.x, data.json.gem[i].pos.y);
                console.log("gem: " + data.json.gem[i].pos.x, data.json.gem[i].pos.y);
            }



            //console.log("MapLv: " + GameManager.indexLv);

            this.loadMapCheck = true;
            console.log("da load map chua: " + this.loadMapCheck);

        });
    }


}



