const { ccclass, property } = cc._decorator;
//const { _decorator, Component, Node,instantiate, director } = cc.;

@ccclass
export default class Tag extends cc.Component {
    public static value = {
        default: 0,
        bullet: 1,
        ground: 2,
        ball: 3,
        box: 4,
        coin: 5,
        gem: 6,
        wood: 7,
        boom: 8,
        expl_boom: 9,
        player: 10,
        axit: 11,
        spine: 12,
        sat: 13
    }
}